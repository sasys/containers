terraform {
  required_providers {
    lxd = {
      source = "terraform-lxd/lxd"
      version = "1.7.1"
    }
  }
}

# provider to connect to infrastructure
provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
}


# The project prefix - we dont use projects because the lxd terra module 
# doesn't support projects at the moment.
# This means we prefix the name with a project name like sasd-clj-atom 
variable "project" {
  description = "The project name."
  default = "sasd"
}

variable "host" {
  description = "The host name of instance"
  default = "atom"
}

variable "name" {
  description = "The name of instance"
  # variables cannot be used here!!
  # default = "${var.project}-${var.host}"
  default = "sasd-atom"
}

variable "storage-name" {
  description = "Name of the storage system"
  default = "sasd-atom-pool"
}

variable "volume-name" {
  description = "Name of the storage system"
  default = "sasd-atom-volume"
}

variable "network-name" {
  description = "Name of the network system"
  default = "sasd-atom-net"
}

variable "profile-name" {
  description = "Name of the profile"
  default = "sasd-atom-profile"
}

variable "installer" {
  description = "An initialisation script for the instance"
  default = "install.sh"
}


variable "ip" {
  description = "An alternate ip address for the instance"  
  default = "10.150.17.1/24"
}


resource "lxd_storage_pool" "sasd-atom-pool" {
  name = "${var.storage-name}"
  driver = "dir"
  config = {
    source = "/var/snap/projects/terra-lxc/storage_pool/sasd-atom-pool"
  }
}


resource "lxd_volume" "sasd-atom-volume" {
  name = "${var.volume-name}"
  pool = "${lxd_storage_pool.sasd-atom-pool.name}"
}


resource "lxd_network" "sasd-atom-net" {
  name = "${var.network-name}"

  config = {
    "ipv4.address" = "${var.ip}" 
    "ipv4.nat"     = "true"
    "ipv6.address" = "fd42:474b:622d:259d::1/64"
    "ipv6.nat"     = "true"
  }
}

resource "lxd_profile" "sasd-atom-profile" {

  name = "${var.profile-name}"

  device {
    name = "eth0"
    type = "nic"

    properties = {
      nictype = "bridged"
      parent  = "${lxd_network.sasd-atom-net.name}"
    }
  }
  device {
    type = "disk"
    name = "root"

    properties = {
      pool = "${lxd_storage_pool.sasd-atom-pool.name}"
      path = "/"
    }
  }

}


resource "lxd_container" "sasd-atom" {
  name      = "${var.name}"
  image     = "ubuntu:21.04"
  ephemeral = false
  type      = "container"
  profiles  = ["${var.profile-name}", "${lxd_profile.gui_profile.name}"]
  device {
    name = "sasd-atom-store"
    type = "disk"
    properties = {
      path = "/opt/"
      source = "${lxd_volume.sasd-atom-volume.name}"
      pool = "${lxd_storage_pool.sasd-atom-pool.name}"
    }
  }                                                                                                                            

  provisioner "local-exec" {
    command = "echo ${var.name}"
  }

  provisioner "local-exec" {
    command = "lxc file push  ${var.installer} ${var.name}/root/./${var.installer}"
  }

  provisioner "local-exec" {
    command = "lxc exec ${var.name} chmod ugo+x ./${var.installer}" 
  }

  provisioner "local-exec" {
    command = "lxc exec ${var.name} sudo ./${var.installer}"
  }


}
