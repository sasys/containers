# Terraform For Clojure And Atom 

This repository is a terraform configuration set to initialise an lxc instance for developing and testing clojure and clojure script.  For example we can build the clj-atom container and then inside the container we can clone out the sa-draw repository and then test it.

Before building the instance, make sure that your project is set to include the features.profiles false so that the project can share the default profiles.  We need this because as of the time of writing we can only build containers in the default project.  This means we will need to move the container to the required project afterwards.  However, lxc doesnt seem to provide for profiles to be moved between projects so when we move the container to the required project it needs to have the built profiles available.

This is not great and will be fixed when the terraform module has project support. 

The above applies to the volumes setting as well.
For example, if the target project is sasd the:

```
  lxc project set sasd features.profiles=false
  lxc project set sasd features.storage.volumes=false
```

After the terraform apply completes, move the container to the target like:

```
  lxc start clj-atom --project sasd
```

```
  terraform init
  terraform apply
```

Then, open bash in the container
```
  lxc exec clj-atom bash
```
 
Then, inside the container:

```
  git clone https://sbnlocean@bitbucket.org/sasys/sa-draw.git
  clojure -A:test-cljs 

``` 

In this container atom will work.  As we are running as root in the container we need to add no-sandbox:

```
  atom --no-sandbox
```


We can run chrome:

```
  google-chrome --no-sandbox
```

We can run firefox
```
  firefox
```


## NOTES

* We cant use projects in the current release of the lxd terraform driver.  This is a pain because the lxc command line doesn't seem to provide for profiles to be copied.  So, if we try to copy the container into a new project, we cnt take the profile with it, the copy fails.  

