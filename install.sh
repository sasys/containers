#! /bin/bash

#
# Installer script for Clojure dev and test
#
#

#Set up user and groups first

#Local directories
#Locations
TOOLS=tools
PROJECTS=projects
PROJECT=sa-draw

cd $HOME
mkdir $TOOLS
mkdir $PROJECTS


# A local tools director
mkdir tools
echo 'export TOOLS=$HOME/tools' | tee -a ~/.bashrc
echo 'export PATH=$PATH:$TOOLS' | tee -a ~/.bashrc
source ~/.bashrc

sudo apt update

# basic tools for other installs
sudo apt --assume-yes install software-properties-common apt-transport-https wget

# atom editor
wget -q https://packagecloud.io/AtomEditor/atom/gpgkey -O- | sudo apt-key add -
sudo add-apt-repository  "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" -y
sudo apt --assume-yes install atom

# X11 Apps to sanity test X
# sudo apt --assume-yes install x11-apps

# Java
wget https://download.java.net/java/GA/jdk17.0.2/dfd4a8d0985749f896bed50d7138ee7f/8/GPL/openjdk-17.0.2_linux-x64_bin.tar.gz
tar xvf openjdk-17.0.2_linux-x64_bin.tar.gz
sudo mv jdk-17.0.2/ /opt/jdk-17/
echo 'export JAVA_HOME=/opt/jdk-17' | tee -a ~/.bashrc
echo 'export PATH=$PATH:$JAVA_HOME/bin '|tee -a ~/.bashrc
source ~/.bashrc

# Clojure
sudo apt-get update
sudo apt-get install rlwrap
curl -O https://download.clojure.org/install/linux-install-1.11.0.1100.sh
chmod +x linux-install-1.11.0.1100.sh
sudo ./linux-install-1.11.0.1100.sh

# Joker (linter)
wget https://github.com/candid82/joker/releases/download/v1.0.0/joker-1.0.0-linux-amd64.zip
unzip joker-1.0.0-linux-amd64.zip 
mv joker $TOOLS

 
# Firefox
sudo apt --assume-yes install firefox

# Chrome used for testing
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt --assume-yes install -f

# Node
sudo apt --assume-yes install nodejs

# npm
sudo apt --assume-yes install npm

# http-server
npm install -g http-server

# Set up the project
cd $PROJECTS
git clone https://sbnlocean@bitbucket.org/sasys/sa-draw.git
cd $PROJECT

# Run the tests
clojure -A:test-cljs




